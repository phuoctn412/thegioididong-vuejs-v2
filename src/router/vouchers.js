// Import component
import CreateVoucher from "../views/Admins/Vouchers/CreateVoucher.vue";

export default [
  {
    path: "/admins/vouchers/create",
    name: "CreateVoucher",
    component: CreateVoucher,
  },
];
