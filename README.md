# vue-economic

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
# thegioididong-vuejs-v2

The gioi di dong E-comm (SPA) using Vuejs 2 + Laravel 8 
This is a test application.
# Admin account: 
Email: phuoc04012000@gmail.com
Password: ngocphuocha