import Api from "../../apis/Api";
import * as types from "../mutation-types/comments";

const commentModule = {
  namespaced: true,
  state: {
    ratingPoint: 0, // rating product
    isEmptyComment: true,
  },
  getters: {},

  actions: {
    // set rating of product
    handleSetRatingProduct({ commit }, point) {
      commit(types.SET_RATING, point);
    },
    checkIsEmptyComment({ commit }, boolean) {
      commit(types.SET_EMPTY_COMMENT, boolean);
    },
    //handle get comments from user
    getCommentsOfProduct({}, productId) {
      return Api().get(`/products/${productId}/comments`);
    },

    // user comment product ordered
    storeComment({}, formData) {
      return Api().post(`/products/${formData.productId}/comments`, formData);
    },

    // get detail comment
    getDetailComments({}, payload) {
      return Api().get(
        `users/${payload.userId}/products/${payload.productId}/comments`
      );
    },

    //update a current comment and rating
    updateCommentAndRating({}, formData) {
      return Api().put(`/comments/ratings/update`, formData);
    },
  },
  mutations: {
    [types.SET_RATING](state, point) {
      state.ratingPoint = point;
    },

    [types.SET_EMPTY_COMMENT](state, boolean) {
      state.isEmptyComment = boolean;
    },
  },
};
export default commentModule;
