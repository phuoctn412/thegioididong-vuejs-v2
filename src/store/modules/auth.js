import { ROLE } from "@/consts";
import router from "@/router";
import Api from "../../apis/Api"; // Api config

const namespaced = true;
const state = {
  auth: {
    isAuthenticated: false,
    role: ROLE.GUEST,
  },
  userInfo: {
    id: "",
    email: "",
    name: "",
  },
};
const getters = {
  getRole: (state) => state.auth.role,
  isAuthenticated: (state) => state.auth.isAuthenticated,
};

const actions = {
  handleRegister({}, formRegister) {
    return Api().post("/register", formRegister);
  },

  async handleLogout({ commit }) {
    const response = await Api().post("/logout");
    commit({
      type: "TOGGLE_AUTH",
      status: false,
      role: ROLE.GUEST,
    });
    localStorage.removeItem("token");
    localStorage.clear();
    router.replace({ name: "Login" });
  },
};
const mutations = {
  TOGGLE_AUTH(state, payload) {
    state.auth.isAuthenticated = payload.status;
    state.auth.role = payload.role;
  },
  UPDATE_USER_INFO(state, payload) {
    state.userInfo.id = payload.id;
    state.userInfo.email = payload.email;
    state.userInfo.name = payload.name;
  },
};

export default {
  namespaced,
  state,
  getters,
  mutations,
  actions,
};
