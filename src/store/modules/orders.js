import * as types from "../mutation-types/order-types";
import Api from "../../apis/Api";

const ordersModule = {
  namespaced: true,
  state: {
    defaultOrderInformation: {}, // store default info if we want to reset default info
    orderInformation: {}, // info send to backend
  },

  actions: {
    setDefaultOrderAddress({ commit }, orderAddressInfomation) {
      commit(types.SET_DEFAULT_ORDER_ADDRESS, orderAddressInfomation);
    },

    getOrderAddress({}, userId) {
      return Api().get(`/users/${userId}/profiles`);
    },

    changeOrderAddress({ commit }, formOrder) {
      commit(types.CHANGE_ORDER_ADDRESS, formOrder);
    },

    restoreOrderAddress({ commit }) {
      commit(types.RESTORE_ORDER_ADDRESS);
    },

    // Store order data to resources
    confirmOrder({}, orderData) {
      return Api().post("/orders", orderData);
    },

    getOrderOfUser({}, payload) {
      return Api().get(`/users/${payload.userId}/orders/?page=${payload.page}`);
    },

    getOrderDetails({}, id) {
      return Api().get(`orders/${id}`);
    },
	
    // Get all orders in dashboard admin
    getAllOrders({}, payload = "") {
      return Api().get(`admins/orders?status=${payload.status}&userInfo=${payload.userInfo}`);
    }
  },
  mutations: {
    [types.SET_DEFAULT_ORDER_ADDRESS](state, orderAddressInformation) {
      state.defaultOrderInformation = orderAddressInformation;

      if (JSON.stringify(state.orderInformation) === "{}") {
        console.log(135);
        state.orderInformation = orderAddressInformation;
      }
    },

    [types.CHANGE_ORDER_ADDRESS](state, formOrder) {
      state.orderInformation = formOrder;
    },

    [types.RESTORE_ORDER_ADDRESS](state) {
      state.orderInformation = state.defaultOrderInformation;
    },
  },
};

export default ordersModule;
