// Import api
import Api from "../../apis/Api";
// Import const mutation
import * as types from "../mutation-types/voucher-types";

const voucherModules = {
  namespaced: true,

  state: {
    voucherCode: null,
    isInvalidVoucher: true,
    validVoucher: {},
    message: "",
    canApplyVoucher: false,
  },

  getters: {
    getVoucherCode(state) {
      return state.voucherCode;
    },
  },

  actions: {
    // Store new voucher to resources (Admin)
    createVoucher({}, formData) {
      return Api().post("admins/vouchers", formData);
    },

    // Select voucher to get discount from checkout (User)
    selectVoucherCode({ commit }, voucherCode) {
      commit(types.UPDATE_VOUCHER_CODE, voucherCode);
    },

    setVoucherCodeIsNullable({ commit }) {
      commit(types.SET_VOUCHER_NULLABLE);
    },

    // Cancel select voucher from checkout (User)
    cancelVoucherCode({ commit }) {
      commit(types.SET_VOUCHER_NULLABLE);
    },

    async checkVoucherCodeIsInvalid({ commit, dispatch }, voucherCode) {
      const response = await Api().get(`/is-invalid-voucher/${voucherCode}`);
      commit(types.UPDATE_VOUHCHER_STATUS, response.data.isInvalid);

      // if voucher isn't invalid
      if (response.data.isInvalid === false) {
        const repo = await dispatch("getVoucherDetail");
        commit(types.SET_VALID_VOUCHER, repo.data);
      }
    },

    // create get api to get a voucher
    getVoucherDetail({ state }) {
      let voucherCode = state.voucherCode;
      return Api().get(`/vouchers/${voucherCode}`);
    },

    // get voucher code with min price
    getVoucherCodeWithMinPrice({}, price) {
      return Api().get(`/vouchers-with-price/${price}`);
    },

    //update message invalid voucher
    updateMessageInvalidVoucher({ commit }, message) {
      commit(types.UPDATE_MESSAGE_VOUCHER_STATUS, message);
    },

    updateStatusApplyVoucher({ commit }, boolean) {
      commit(types.UPDATE_STATUS_CAN_APPLY_VOUCHER, boolean);
    },
  },

  mutations: {
    // Update current voucher
    [types.UPDATE_VOUCHER_CODE](state, voucherCode) {
      state.voucherCode = voucherCode;
    },

    // Set voucher to nullable
    [types.SET_VOUCHER_NULLABLE](state) {
      state.voucherCode = null;
    },

    [types.UPDATE_VOUHCHER_STATUS](state, boolean) {
      state.isInvalidVoucher = boolean;
    },

    [types.SET_VALID_VOUCHER](state, voucher) {
      state.validVoucher = voucher;
    },

    [types.UPDATE_MESSAGE_VOUCHER_STATUS](state, message) {
      state.message = message;
    },

    [types.UPDATE_STATUS_CAN_APPLY_VOUCHER](state, boolean) {
      state.canApplyVoucher = boolean;
    },
  },
};

export default voucherModules;
