import Product from "../views/Products/Products";
import ProductDetail from "../views/Products/ProductDetail";

export default [
  {
    path: "/products",
    name: "Products",
    component: Product,
  },
  {
    path: "/products/:id",
    name: "ProductDetails",
    component: ProductDetail,
    props: true,
  },
];
