import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

// import modules
import auth from "./modules/auth";
import products from "./modules/products";
import carts from "./modules/carts";
import orders from "./modules/orders";
import vouchers from "./modules/vouchers";
import comments from "./modules/comments";

export default new Vuex.Store({
  modules: {
    AUTH: auth,
    PRODUCTS: products,
    CARTS: carts,
    ORDERS: orders,
    VOUCHERS: vouchers,
    COMMENTS: comments,
  },
  plugins: [createPersistedState()],
});
