import * as types from "../mutation-types/cart-types";

const cartsModule = {
  namespaced: true,

  state: {
    carts: [],
    checkoutStatus: null,
    isItemAddToCart: false,
    totalPriceAfterApplyVoucher: 0,
  },
  getters: {
    getCartItems: (state) => {
      return state.carts.map((item) => {
        return {
          product_id: item.product_id,
          name: item.name,
          quantity: item.quantity,
          price: item.price,
          subTotalPrice: item.quantity * item.price,
          color_id: item.color_id,
          color_name: item.color_name,
        };
      });
    },

    getTotalPrice: (state) => {
      return state.carts.reduce(
        (total, item) => total + item.quantity * item.price,
        0
      );
    },
  },
  actions: {
    // Add item to cart
    addToCart({ commit }, product) {
      commit(types.ADD_TO_CART, product);
    },

    // Update item cart
    updateCartItem({ commit }, payload) {
      commit(types.UPDATE_CART_ITEM, payload);
    },

    // Increment item cart if exist
    incrementItemCart({ commit }, product_id) {
      commit(types.INCREMENT_CART_ITEM, product_id);
    },
    // Decrement item cart if exist
    decrementItemCart({ commit }, product_id) {
      commit(types.DECREMENT_CART_ITEM, product_id);
    },

    deleteItemCart({ commit }, product_id) {
      let result = confirm("Bạn có muốn xóa vật phẩm nảy khỏi giỏ hàng?");
      if (result) {
        commit(types.REMOVE_CART_ITEM, product_id);
        alert("Remove item from cart successful!");
      }
    },

    deleteAllItemCart({ commit }) {
      commit(types.REMOVE_ALL_CART_ITEM);
    },

    setTotalPriceAfterApplyVoucher({ commit }, price) {
      commit(types.SET_TOTAL_PRICE_AFTER_APPLY_VOUCHER, price);
    },
  },
  mutations: {
    [types.ADD_TO_CART](state, product) {
      const product_id = product.id;
      const name = product.name;
      const price = product.price;
      const color_id = product.color_id;
      const color_name = product.color_name;

      // filter result
      let result = state.carts.filter(
        (e) => e.product_id === product_id && e.color_id === color_id
      );
      console.log("result:", result);

      if (result.length === 0) {
        state.carts.push({
          product_id,
          name,
          price,
          quantity: 1,
          color_id,
          color_name,
        });

        // success push to cart
        state.isItemAddToCart = false;
      } else {
        // failed push to cart
        state.isItemAddToCart = true;
      }
    },

    [types.UPDATE_CART_ITEM](state, payload) {
      const item = state.carts.find(
        (item) =>
          item.product_id === payload.product_id &&
          item.color_id === payload.color_id
      );
      Object.assign(item, payload);
    },

    [types.INCREMENT_CART_ITEM](state, payload) {
      // Increase item cart state
      state.carts.forEach((item) => {
        if (
          item.product_id === payload.product_id &&
          item.color_id === payload.color_id
        ) {
          return item.quantity++;
        }
      });
    },

    [types.DECREMENT_CART_ITEM](state, payload) {
      // Decrement item cart state
      state.carts.forEach((item, index) => {
        if (
          item.product_id === payload.product_id &&
          item.color_id === payload.color_id
        ) {
          item.quantity--;
          if (item.quantity === 0) {
            let result = confirm("Bạn có muốn xóa vật phẩm nảy khỏi giỏ hàng?");
            if (result) {
              return state.carts.splice(index, 1); // remove item at index where item.product_id = product_id
            } else {
              return (item.quantity = 1);
            }
          }
        }
      });
    },
    [types.REMOVE_CART_ITEM](state, payload) {
      // Remove item when quantity = 0 or click button remove
      state.carts.forEach((item, index) => {
        if (
          item.product_id === payload.product_id &&
          item.color_id === payload.color_id
        ) {
          return state.carts.splice(index, 1); // remove item at index where item.product_id = product_id
        }
      });
    },

    [types.REMOVE_ALL_CART_ITEM](state) {
      state.carts = [];
    },

    [types.SET_TOTAL_PRICE_AFTER_APPLY_VOUCHER](state, price) {
      state.totalPriceAfterApplyVoucher = price;
    },
  },
};

export default cartsModule;
