import router from "../index";
import store from "../../store";
import {ROLE} from "../../consts";

const checkLogin = (to, from, next) => {
    if (!localStorage.getItem("token")) {
        next();
    } else {
        router.go(-1);
    }
};

const checkEmtpyCart = (to, from, next) => {
    if (store.getters["CARTS/getCartItems"].length !== 0) {
        next();
    } else {
        router.push({name: "NotFound"});
    }
};

const checkIsSuperAdminRole = (to, from, next) => {
    if (store.getters["AUTH/getRole"] === ROLE.SUPER_ADMIN) {
        next();
    } else {
        console.log("1212");
        console.log(store.getters["AUTH/getRole"]);
        router.push({name: "NotFound"});
        // router.go(-1);
    }
};

export {checkLogin, checkEmtpyCart, checkIsSuperAdminRole};
